var tty = require('tty.js');
var spawn = require('child_process').spawn;

var child = spawn(
	'udevd',
	[''],
	{
	    detached: true,
	    stdio: [ 'ignore', 'ignore', 'ignore' ]
	}
	);

var child1 = spawn(
	'udevadm',
	['trigger'],
	{
	    detached: true,
	    stdio: [ 'ignore', 'ignore', 'ignore' ]
	}
	);

// Simple tty.js in app mode
var app = tty.createServer({
  shell: 'bash',
  users: {
    admin: 'admin'
  },
  port: process.env.PORT || '8080'
});

app.listen();
